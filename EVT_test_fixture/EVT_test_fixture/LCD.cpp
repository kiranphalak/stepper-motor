/*
 * CPPFile1.cpp
 *
 * Created: 10/17/2018 3:52:34 PM
 *  Author: kphalak
 */ 

//************************** Display Functions  *************************//
#include "LCD.h"
#include <SoftwareSerial.h>

SoftwareSerial OpenLCD(6,7);//RX, TX
void ShowScreen(int screen_number, int param)
{
	OpenLCD.write('|');
	OpenLCD.write('-');
	switch(screen_number)
	{
		case screen_welcome:
		OpenLCD.print("Select the Test "); //For 16x2 LCD
		OpenLCD.print("<              >"); //For 16x2 LCD
		break;
		case screen_set_parameters:
		OpenLCD.print(" Set Parameters "); //For 16x2 LCD
		OpenLCD.print("EXIT         SET"); //For 16x2 LCD
		break;
		case screen_angle_param:
		OpenLCD.print("Step Angle: "); //For 16x2 LCD
		OpenLCD.print(param);
		OpenLCD.write(254);
		OpenLCD.write(128 + 64 + 0);
		OpenLCD.print("<  BACK   OK   >");
		break;
		case screen_cycle_param:
		if(param > 9999)
		{
			OpenLCD.print("Num Cycles:"); //For 16x2 LCD
		}
		else
		{
			OpenLCD.print("Num Cycles: "); //For 16x2 LCD
		}
		OpenLCD.print(param);
		OpenLCD.write(254);
		OpenLCD.write(128 + 64 + 0);
		OpenLCD.print("<  BACK   OK   >");
		break;
		case screen_step_wait_param:
		OpenLCD.print("Step Wait: "); //For 16x2 LCD
		OpenLCD.print(param);
		OpenLCD.write(254);
		OpenLCD.write(128 + 64 + 0);
		OpenLCD.print("<  BACK   OK   >");
		break;
		case screen_cycle_wait_param:
		OpenLCD.print("Cycle Wait: "); //For 16x2 LCD
		OpenLCD.print(param);
		OpenLCD.write(254);
		OpenLCD.write(128 + 64 + 0);
		OpenLCD.print("<  BACK   OK   >");
		break;
		case screen_init_position:
		OpenLCD.print(" Init  Position ");
		OpenLCD.print("<  BACK   OK   >");
		break;
		case screen_running_life_test:
		OpenLCD.print("Finished Cycles:");
		OpenLCD.write(254);
		OpenLCD.write(128 + 64 + 3);
		OpenLCD.print(param);
		OpenLCD.print(" of ");
		OpenLCD.print(life_test_cycles_finished + life_test_max_cycles);
		break;
		case screen_test_paused:
		OpenLCD.print("  Resume Test?  ");
		OpenLCD.print("NO           YES");
		break;
		case screen_confirm_test_start:
		OpenLCD.print("  Start  Test?  ");
		OpenLCD.print("NO           YES");
		break;
		case screen_renew_lifetest:
		OpenLCD.print("Cycles Done :");
		OpenLCD.print(param);
		OpenLCD.write(254);
		OpenLCD.write(128 + 64 + 0);
		OpenLCD.print("EXIT         ADD");
		break;
		case screen_life_test_add_cycles:
		OpenLCD.print("Add Cycles :");
		OpenLCD.print(param);
		OpenLCD.write(254);
		OpenLCD.write(128 + 64 + 0);
		OpenLCD.print("<         OK   >");
		break;
		default:
		OpenLCD.print(" Unknown  Error ");
		break;
	}
}

void ShowText(String str)
{
	OpenLCD.write('|');
	OpenLCD.write('-');
	OpenLCD.print(str); //For 16x2 LCD
}

void TurnOnBackLight()
{
	OpenLCD.write('|');
	OpenLCD.write(128+19);//Red
	OpenLCD.write('|');
	OpenLCD.write(158+19);//Green
	OpenLCD.write('|');
	OpenLCD.write(188+19);//Blue
}

void ShowScreenTestSelection(int selection)
{
	OpenLCD.write('|');
	OpenLCD.write('-');
	switch(selection)
	{
		case select_a:
		//OpenLCD.print("Press SELECT for"); //For 16x2 LCD
		OpenLCD.print("   Life Test    "); //For 16x2 LCD
		OpenLCD.print("        OK     >"); //For 16x2 LCD
		break;
		case select_b:
		//OpenLCD.print("Press SELECT for"); //For 16x2 LCD
		OpenLCD.print("   Stress Test  "); //For 16x2 LCD
		OpenLCD.print("<       OK     >"); //For 16x2 LCD
		break;
		case select_c:
		//OpenLCD.print("Press SELECT for"); //For 16x2 LCD
		OpenLCD.print("  Puncture Test "); //For 16x2 LCD
		OpenLCD.print("<       OK      "); //For 16x2 LCD
		break;
	}
}

//********************* Main Application  ***************************//
void setup() {
	// put your setup code here, to run once:
	Serial.begin(9600); //Start serial communication at 9600 for debug statements
	Serial.println("EVT Test Code");
	OpenLCD.begin(9600);
	TurnOnBackLight();
	ShowScreen(screen_welcome,0);
	SetEDPins();
}