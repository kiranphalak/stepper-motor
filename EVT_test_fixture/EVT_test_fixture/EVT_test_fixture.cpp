﻿/*Begining of Auto generated code by Atmel studio */
#include <Arduino.h>

/*End of auto generated code by Atmel studio */

// EVT test fixture operation
// Ideal user experience:
// Once the power is turned ON, Display shows splash screen 
//It enters a mode where its waiting for button press
//left will go to previous mode and right will go to next mode (new text will be displayed for that mode)
//select will change the state

#include <SoftwareSerial.h>
#include "Functions.h"
#include "LCD.h"

//SoftwareSerial OpenLCD(6,7);//RX, TX

//************************** Variables and Enumerators  *************************//
int life_test_step_angle = 180;
int life_test_max_cycles = 1000;
int life_test_step_wait = 10;
int life_test_cycle_wait = 1000;
int life_test_cycles_finished = 0;
bool motor_direction_forward = true;


//****************** Button Definitions and Functions  *************************//

class Button
{
  int buttonPin;
  int buttonState;
  int lastbuttonState;
  public:
  
  Button(int pin)
  {
    buttonPin = pin;
    buttonState = HIGH;
    lastbuttonState = HIGH;
    
    pinMode(buttonPin,INPUT);
    digitalWrite(buttonPin, HIGH);
  } 
  bool IsPressed()
  {
    buttonState = digitalRead(buttonPin);
    if(buttonState != lastbuttonState)
    {
      lastbuttonState = buttonState;
      if(buttonState == LOW)
      {
        return true;
      }
    }
    return false;
  }
  bool IsLow()
  {
    if(digitalRead(buttonPin) == LOW)
    {
      return true; 
    }
    return false;
  }
};

Button btn_prev(2);
Button btn_back(3);
Button btn_select(4);
Button btn_next(5);

//****************** Stepper Motor Definitions and Functions  *************************//
#define dir 12
#define stp 11
#define MS2 10
#define EN  9
#define MS1 8

void SetEDPins()
{
  pinMode(stp, OUTPUT);
  pinMode(dir, OUTPUT);
  pinMode(MS1, OUTPUT);
  pinMode(MS2, OUTPUT);
  pinMode(EN, OUTPUT);
  resetEDPins(); 
}
//Reset Easy Driver pins to default states
void resetEDPins()
{
  digitalWrite(stp, LOW);
  digitalWrite(dir, LOW);
  digitalWrite(MS1, LOW);
  digitalWrite(MS2, LOW);
  digitalWrite(EN, HIGH);
}

//Default micro step mode function
void StepForward(int steps,int wait)
{
  digitalWrite(dir, LOW); //Pull direction pin low to move "forward"
  StepMovement(steps,wait);
}

//Reverse default micro step mode function
void StepBackward(int steps,int wait)
{
  digitalWrite(dir, HIGH); //Pull direction pin high to move "reverse"
  StepMovement(steps,wait);
}

void StepMovement(int steps,int wait)
{
  for(int x = 1; x < steps; x++)  //Loop the stepping enough times for motion to be visible
  {
    digitalWrite(stp,HIGH); //Trigger one step
    delay(wait);
    digitalWrite(stp,LOW); //Pull step pin low so it can be triggered again
    delay(wait);
  }
}

enum _test_options
{
  invalid_option,
  select_a,
  select_b,
  select_c,
  last_test
} current_option = invalid_option;

enum _life_test_param
{
  invalid_param,
  step_angle,
  step_cycles,
  step_wait_param,
  cycle_wait_param,
  init_position_param,
  last_param
} current_life_test_param = step_angle;

enum
{
  screen_welcome,
  screen_set_parameters,
  screen_angle_param,
  screen_cycle_param,
  screen_step_wait_param,
  screen_cycle_wait_param,
  screen_init_position,
  screen_confirm_test_start,
  screen_running_life_test,
  screen_test_paused,
  screen_renew_lifetest,
  screen_life_test_add_cycles
} ScreenNum = screen_welcome;

enum
{
  test_selection,
  confirm_selection,
  setting_test_params,
  confirm_test_start,
  running_test,
  standby_test,
  restart_test,
  exiting_test
} current_state = test_selection;

//************************** Display Functions  *************************//

void ShowScreen(int screen_number, int param)
{
	OpenLCD.write('|');
	OpenLCD.write('-');
	switch(screen_number)
	{
		case screen_welcome:
		OpenLCD.print("Select the Test "); //For 16x2 LCD
		OpenLCD.print("<              >"); //For 16x2 LCD
		break;
		case screen_set_parameters:
		OpenLCD.print(" Set Parameters "); //For 16x2 LCD
		OpenLCD.print("EXIT         SET"); //For 16x2 LCD
		break;
		case screen_angle_param:
		OpenLCD.print("Step Angle: "); //For 16x2 LCD
		OpenLCD.print(param);
		OpenLCD.write(254);
		OpenLCD.write(128 + 64 + 0);
		OpenLCD.print("<  BACK   OK   >");
		break;
		case screen_cycle_param:
		if(param > 9999)
		{
			OpenLCD.print("Num Cycles:"); //For 16x2 LCD
		}
		else
		{
			OpenLCD.print("Num Cycles: "); //For 16x2 LCD
		}
		OpenLCD.print(param);
		OpenLCD.write(254);
		OpenLCD.write(128 + 64 + 0);
		OpenLCD.print("<  BACK   OK   >");
		break;
		case screen_step_wait_param:
		OpenLCD.print("Step Wait: "); //For 16x2 LCD
		OpenLCD.print(param);
		OpenLCD.write(254);
		OpenLCD.write(128 + 64 + 0);
		OpenLCD.print("<  BACK   OK   >");
		break;
		case screen_cycle_wait_param:
		OpenLCD.print("Cycle Wait: "); //For 16x2 LCD
		OpenLCD.print(param);
		OpenLCD.write(254);
		OpenLCD.write(128 + 64 + 0);
		OpenLCD.print("<  BACK   OK   >");
		break;
		case screen_init_position:
		OpenLCD.print(" Init  Position ");
		OpenLCD.print("<  BACK   OK   >");
		break;
		case screen_running_life_test:
		OpenLCD.print("Finished Cycles:");
		OpenLCD.write(254);
		OpenLCD.write(128 + 64 + 3);
		OpenLCD.print(param);
		OpenLCD.print(" of ");
		OpenLCD.print(life_test_cycles_finished + life_test_max_cycles);
		break;
		case screen_test_paused:
		OpenLCD.print("  Resume Test?  ");
		OpenLCD.print("NO           YES");
		break;
		case screen_confirm_test_start:
		OpenLCD.print("  Start  Test?  ");
		OpenLCD.print("NO           YES");
		break;
		case screen_renew_lifetest:
		OpenLCD.print("Cycles Done:");
		OpenLCD.print(param);
		OpenLCD.write(254);
		OpenLCD.write(128 + 64 + 0);
		OpenLCD.print("EXIT         ADD");
		break;
		case screen_life_test_add_cycles:
		OpenLCD.print("Add Cycles:");
		OpenLCD.print(param);
		OpenLCD.write(254);
		OpenLCD.write(128 + 64 + 0);
		OpenLCD.print("<         OK   >");
		break;
		default:
		OpenLCD.print(" Unknown  Error ");
		break;
	}
}

void ShowText(String str)
{
	OpenLCD.write('|');
	OpenLCD.write('-');
	OpenLCD.print(str); //For 16x2 LCD
}

void TurnOnBackLight()
{
	OpenLCD.write('|');
	OpenLCD.write(128+19);//Red
	OpenLCD.write('|');
	OpenLCD.write(158+19);//Green
	OpenLCD.write('|');
	OpenLCD.write(188+19);//Blue
}

void ShowScreenTestSelection(int selection)
{
	OpenLCD.write('|');
	OpenLCD.write('-');
	switch(selection)
	{
		case select_a:
		//OpenLCD.print("Press SELECT for"); //For 16x2 LCD
		OpenLCD.print("   Life Test    "); //For 16x2 LCD
		OpenLCD.print("        OK     >"); //For 16x2 LCD
		break;
		case select_b:
		//OpenLCD.print("Press SELECT for"); //For 16x2 LCD
		OpenLCD.print("   Stress Test  "); //For 16x2 LCD
		OpenLCD.print("<       OK     >"); //For 16x2 LCD
		break;
		case select_c:
		//OpenLCD.print("Press SELECT for"); //For 16x2 LCD
		OpenLCD.print("  Puncture Test "); //For 16x2 LCD
		OpenLCD.print("<       OK      "); //For 16x2 LCD
		break;
	}
}

//********************* Main Application  ***************************//
void setup() {
	// put your setup code here, to run once:
	Serial.begin(9600); //Start serial communication at 9600 for debug statements
	Serial.println("EVT Test Code");
	OpenLCD.begin(9600);
	//Rolling reset
	OpenLCD.write('|'); //Send setting character
	OpenLCD.write(8); //Software reset of the system, takes 250ms max
	delay(1500);
	TurnOnBackLight();
	ShowScreen(screen_welcome,0);
	SetEDPins();
}

void loop() {
  // put your main code here, to run repeatedly:
  switch(current_state)
  {
	case test_selection:
		SelectTest();
		break;
	case confirm_selection:
		ConfirmSelection();
		break;
	case setting_test_params:
		SetTestParameters();
		break;
	case confirm_test_start:
		ConfirmTest();
		break;
	case running_test:
		RunTest();
		break;
	case standby_test:
		HandleEndOfTest();
		break;
	case restart_test:
		SetRestartTest();
		break;
  }
  
}

void SetRestartLifeTest()
{
  if(current_state != restart_test) return;
  int additional_cycles = life_test_max_cycles;
  int n = 1;
  while(current_state == restart_test){
    if(btn_next.IsLow())
    {
      additional_cycles = additional_cycles + n*10;
      if(additional_cycles > 25000) additional_cycles = 25000;
      ShowScreen(screen_life_test_add_cycles,additional_cycles);
      delay(200);     
    }
    else if (btn_prev.IsLow())
    {
      additional_cycles = additional_cycles - n*10;
      if(additional_cycles < 10) additional_cycles = 10;
      ShowScreen(screen_life_test_add_cycles,additional_cycles);
      delay(200);
    }
    else if (btn_select.IsPressed())
    {
      current_state = running_test;
      life_test_max_cycles = additional_cycles;
    }
	else
	{
		n = 1;
	}
	n++;
  }
}

void SetRestartTest()
{
  if(current_state != restart_test) return;
  switch(current_option)
  {
    case select_a:
        SetRestartLifeTest();
        break;
      case select_b:
        break;
      case select_c:
        break;
      default:
        break;
  }
}

void HandleEndOfTest()
{
  if(current_state != standby_test) return;
  while(current_state == standby_test){
    if(btn_next.IsPressed())
    {
      current_state = restart_test;
      ShowScreen(screen_life_test_add_cycles,life_test_max_cycles);
      while(btn_next.IsLow()){}      
    }
    else if (btn_prev.IsPressed())
    {
      current_state = test_selection;
      current_option = invalid_option;
      ShowScreen(screen_welcome,0);
	  motor_direction_forward = true;
	  current_life_test_param = step_angle;
	  life_test_cycles_finished = 0;
      return;
    }
  }
}

void RunLifeTest()
{
  digitalWrite(EN, LOW);
  int n = 1;
  while(current_state == running_test)
  {
	if(motor_direction_forward == true)
	{
		StepForward(life_test_step_angle/1.8,life_test_step_wait);
	}
	else
	{
		StepBackward(life_test_step_angle/1.8,life_test_step_wait);		    
	}
    //print number
    OpenLCD.write(254);
    OpenLCD.write(128 + 64 + 3);
    OpenLCD.print(n+life_test_cycles_finished);
    OpenLCD.print(" of ");
    OpenLCD.print(life_test_max_cycles+life_test_cycles_finished);
	motor_direction_forward = !motor_direction_forward;
    delay(life_test_cycle_wait);
	if(btn_back.IsLow())
    {
		ShowScreen(screen_test_paused,0);
		while(1)
		{
			if(btn_next.IsPressed())
			{
				break;
			}
			if(btn_prev.IsPressed())
			{
				current_state = test_selection;
				current_option = invalid_option;
				ShowScreen(screen_welcome,0);
				motor_direction_forward = true;
				current_life_test_param = step_angle;
				life_test_cycles_finished = 0;
				return;
			}
		}
		ShowScreen(screen_running_life_test, n + life_test_cycles_finished);
    }
    if(n==life_test_max_cycles)
    {
      current_state = standby_test;      
      life_test_cycles_finished = life_test_cycles_finished + life_test_max_cycles;
      ShowScreen(screen_renew_lifetest,life_test_cycles_finished);
    }
    n++;
  }   
}

void RunTest()
{
  if(current_state != running_test) return;
  switch(current_option)
  {
    case select_a:
        ShowScreen(screen_running_life_test,life_test_cycles_finished);
        RunLifeTest();
        break;
      case select_b:
        break;
      case select_c:
        break;
      default:
        break;
  }
}

void ConfirmTest()
{
  while(current_state == confirm_test_start)
  {
    if(btn_next.IsPressed())
    {
      current_state = running_test;
    }
    else if (btn_prev.IsPressed())
    {
      current_life_test_param = step_angle;
      current_state = test_selection;
      current_option = invalid_option;
      ShowScreen(screen_welcome,0);
      break;
    }
  }
}

void ControlInitialPosition()
{
  if(current_state == setting_test_params)
  {
    ShowScreen(screen_init_position,0);
  }
  while(current_life_test_param == init_position_param)
  {
    if(btn_next.IsPressed())
    {
      digitalWrite(EN, LOW);
      StepForward(5,1);
    }
    else if (btn_prev.IsPressed())
    {
      digitalWrite(EN, LOW);
      StepBackward(5,1);
    }
    else if (btn_select.IsPressed())
    {
      digitalWrite(EN, HIGH);
      current_state = confirm_test_start;
      ShowScreen(screen_confirm_test_start,0);
      return;
    }    
    else if(btn_back.IsPressed())
    {
      current_life_test_param = cycle_wait_param;
      ShowScreen(screen_cycle_wait_param,life_test_cycle_wait);
    }
    digitalWrite(EN, HIGH);
  }
}

void ControlCycleWaitTime()
{
  int cycle_wait = life_test_cycle_wait;
  if(current_state == setting_test_params)
  {
    ShowScreen(screen_cycle_wait_param,cycle_wait);
  }
  while(current_life_test_param == cycle_wait_param)
  {
    if(btn_next.IsPressed())
    {
      cycle_wait = cycle_wait + 100;
      if(cycle_wait >10000) cycle_wait = 10000;
      ShowScreen(screen_cycle_wait_param,cycle_wait);
    }
    else if(btn_prev.IsPressed())
    {
      cycle_wait = cycle_wait - 100;
      if(cycle_wait < 100) cycle_wait = 100;
      ShowScreen(screen_cycle_wait_param,cycle_wait);
    }
    else if(btn_select.IsPressed())
    {
      life_test_cycle_wait = cycle_wait;
      current_life_test_param = init_position_param;
    }    
    else if(btn_back.IsPressed())
    {
      current_life_test_param = step_wait_param;
      ShowScreen(screen_step_wait_param,life_test_step_wait);
    }
  }
}

void ControlStepWaitTime()
{
  int step_wait = life_test_step_wait;
  if(current_state == setting_test_params)
  {
    ShowScreen(screen_step_wait_param,step_wait);
  }
  while(current_life_test_param == step_wait_param)
  {
    if(btn_next.IsPressed())
    {
      step_wait = step_wait + 10;
      if(step_wait >2500) step_wait = 2500;
      ShowScreen(screen_step_wait_param,step_wait);
    }
    else if(btn_prev.IsPressed())
    {
      step_wait = step_wait - 10;
      if(step_wait < 10) step_wait = 10;
      ShowScreen(screen_step_wait_param,step_wait);
    }
    else if(btn_select.IsPressed())
    {
      life_test_step_wait = step_wait;
      current_life_test_param = cycle_wait_param;
    }    
    else if(btn_back.IsPressed())
    {
      current_life_test_param = step_cycles;
      ShowScreen(screen_cycle_param,life_test_max_cycles);
    }
  }
}

void ControlCycleNumber()
{
  int cycles = life_test_max_cycles;
  
  if(current_state == setting_test_params)
  {
    ShowScreen(screen_cycle_param,cycles);
  }
  int n = 1;
  while(current_life_test_param == step_cycles)
  {
    if(btn_next.IsLow())
    {
      cycles = cycles + n*10;
      if(cycles >25000) cycles = 25000;
      ShowScreen(screen_cycle_param,cycles);
      delay(200);
    }
    else if(btn_prev.IsLow())
    {
      cycles = cycles - n*10;
      if(cycles < 10) cycles = 10;
      ShowScreen(screen_cycle_param,cycles);
      delay(200);
    }
    else if(btn_select.IsPressed())
    {
      life_test_max_cycles = cycles;
      current_life_test_param = step_wait_param;
    }    
    else if(btn_back.IsPressed())
    {
      current_life_test_param = step_angle;
      ShowScreen(screen_angle_param,life_test_step_angle);
    }
    else
    {
	    n = 1;
    }
	n++;
  }
}

void ControlStepAngle()
{
  int angle = life_test_step_angle;
  
  if(current_state == setting_test_params)
  {
    ShowScreen(screen_angle_param,angle);
  }  
  while(current_life_test_param == step_angle)
  {
    if(btn_next.IsPressed())
    {
      angle = angle + 18;
      if(angle == 360) angle = 0;
      ShowScreen(screen_angle_param,angle);
    }
    else if(btn_prev.IsPressed())
    {
      angle = angle - 18;
      if(angle == -18) angle = 342;
      ShowScreen(screen_angle_param,angle);
    }
    else if(btn_select.IsPressed())
    {
      life_test_step_angle = angle;
      current_life_test_param = step_cycles;
    }
    else if(btn_back.IsPressed())
    {
      current_state = test_selection;
      current_option = invalid_option;
      ShowScreen(screen_welcome,0);
      break;//exiting step angle loop
    }
  }
}

void SetTestParameters()
{
  while(current_state == setting_test_params)
  {
    switch(current_option)
    {
      case select_a:
        ControlStepAngle();
        ControlCycleNumber();
        ControlStepWaitTime();
        ControlCycleWaitTime();
        ControlInitialPosition();
        return;
      case select_b:
        break;
      case select_c:
        break;
    }
  } 
}

void ConfirmSelection()
{
  while(current_state == confirm_selection)
  {
    if(btn_next.IsPressed())// SET has been pressed
    {
      current_state = setting_test_params;
      break;//confirm_selection loop
    }
    else if(btn_prev.IsPressed())
    {
      current_state = test_selection;
      current_option = invalid_option;
      ShowScreen(screen_welcome,0);
      break;
    }
  }
}

void SelectTest()
{
  while(current_state == test_selection){
    if(btn_next.IsPressed())
    {
      current_option = (_test_options)(current_option+1);
      if(current_option == last_test) current_option = (_test_options)(current_option-1);
      ShowScreenTestSelection(current_option);
    }
    else if (btn_prev.IsPressed())
    {
      current_option = (_test_options)(current_option-1);
      if(current_option == invalid_option) current_option = (_test_options)(current_option + 1);
      ShowScreenTestSelection(current_option);
    }
    else if (btn_select.IsPressed())
    {
      if(current_option != invalid_option) current_state = confirm_selection;
      ShowScreen(screen_set_parameters,0);
    }
  }
}



