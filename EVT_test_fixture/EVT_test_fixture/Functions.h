/*
 * Functions.h
 *
 * Created: 10/16/2018 10:47:35 AM
 *  Author: kphalak
 */ 


#ifndef FUNCTIONS_H_
#define FUNCTIONS_H_

SoftwareSerial OpenLCD(6,7);//RX, TX

//Beginning of Auto generated function prototypes by Atmel Studio
void SetEDPins();
void resetEDPins();
void StepForward(int steps, int wait);
void ReverseStep(int steps, int wait);
void StepMovement(int steps, int wait);
void ShowScreen(int screen_number, int param);
void ShowText(String str);
void TurnOnBackLight();
void ShowScreenTestSelection(int selection);
void SetRestartLifeTest();
void SetRestartTest();
void HandleEndOfTest();
void RunLifeTest();
void RunTest();
void ConfirmTest();
void ControlInitialPosition();
void ControlCycleWaitTime();
void ControlStepWaitTime();
void ControlCycleNumber();
void ControlStepAngle();
void SetTestParameters();
void ConfirmSelection();
void SelectTest();
//End of Auto generated function prototypes by Atmel Studio

#endif /* FUNCTIONS_H_ */