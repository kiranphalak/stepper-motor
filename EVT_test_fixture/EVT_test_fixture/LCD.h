/*
 * LCD.h
 *
 * Created: 10/17/2018 3:51:32 PM
 *  Author: kphalak
 */ 


#ifndef LCD_H_
#define LCD_H_

void ShowScreen(int screen_number, int param);
void ShowText(String str);
void TurnOnBackLight();
void ShowScreenTestSelection(int selection);



#endif /* LCD_H_ */